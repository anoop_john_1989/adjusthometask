`adjust_home_task` is project which exposes the sample dataset through RESTful API which is capable of filtering, grouping and sorting. 
Dataset represents performance metrics (impressions, clicks, installs, spend, revenue) for a given date, advertising channel, 
country and operating system. To store and process data relational database called [SQLite](https://www.sqlite.org) has been chosen.


  * [Getting started](#getting-started)
    * [Requirements](#getting-started-requirements)
    * [Start the project](#start-the-project)
  * [API](#api)
  * [API Use Cases](#Use-Cases)
    
## Getting started

Dataset which is used by the project is presented by the [reference](https://gist.github.com/kotik/93bbded94031a04e46f75cbef23b2ec7).

### Requirements

- Python 3
- Django 2
- Python Virtual Environment(https://docs.python.org/3/tutorial/venv.html)

### Start the project

 - Entrypoint script installs the requirements.txt with assumption Python3 is installed and a python virtual_env is activated
 - Migrates the database.
 - Loads the database with sample data from https://gist.githubusercontent.com/kotik/3baa5f53997cce85cc0336cb1256ba8b/raw/3c2a590b9fb3e9c415a99e56df3ddad5812b292f/dataset.csv
```bash
(virtual_env)$ ./entrypoint.sh
```

### Use-Cases

1) Show the number of impressions and clicks that occurred before the 1st of June 2017,
 broken down by channel and country, sorted by clicks in descending order

```bash
$ curl -X GET 'http://127.0.0.1:8000/metrics/?date_to=2017-06-01&group_by=channel&group_by=country&sort=-clicks'
```

2) Show the number of installs that occurred in May of 2017 on iOS,
 broken down by date, sorted by date in ascending order.


```bash
$ curl -X GET 'http://127.0.0.1:8000/metrics/?date_from=2017-05-01&date_to=2017-05-31&group_by=os&os=ios&group_by=date&sort=-date'
```

3) Show revenue, earned on June 1, 2017 in US, broken down by operating system and
 sorted by revenue in descending order.

```bash
$ curl -X GET 'http://127.0.0.1:8000/metrics/?date_from=2017-06-01&date_to=2017-06-01&group_by=os&sort=-revenue'
'   
```

4) Show CPI and spend for Canada (CA) broken down by channel ordered by CPI in descending order.
 Please think carefully which is an appropriate aggregate function for CPI.

```bash
$ curl -X GET 'http://127.0.0.1:8000/metrics/?group_by=channel&sort=-cpi&country=CA'  
```

## API


As a client of the API, you are able to:

- filter by time range (date_from and date_to), channels, countries, operating systems;
- group by one or more columns (date, channel, country, operating system);
- sort by any column in ascending or descending order;
- use filtering, grouping, sorting at the same time.
- calculate costs per install per entry

The following request's parameters could be use while interaction with API.

| Arguments | Type   |  Required | Description                         | 
| :--------:| :----: | :-------: | ----------------------------------- |
| date_from | Date   |  No       | Date to filter the result from.     |
| date_to   | Date   |  No       | Date to filter the result to.       |
| channel   | String |  No       | Channel of the promotion.           | 
| country   | String |  No       | Count of the promotion.             |
| os        | String |  No       | Operation system of an application. |
| cpi       | Number |  No       | Costs per install                   |
| group_by  | String |  No       | Grouping by or broking down by.     |
| sort      | String |  No       | Sort the result by.                 |
|impressions| Number |  No       | Impressions captured.                 |
| clicks    | Number |  No       | Clicks captured.                 |
| revenue   | Number |  No       | Revenue generated.                 |

There is the example of the request:

```bash
$ curl -X GET 'http://127.0.0.1:8000/metrics/?date_from=2017-05-01&date_to=2017-05-17'
```

If you want to group by multiple times, use the following pattern:

```bash
$ curl -X GET 'http://127.0.0.1:8000/metrics/?group_by=country&group_by=channel'
```

To sort in ascending order, use the following pattern:

```bash
$ curl -X GET 'http://127.0.0.1:8000/metrics/?sort=clicks'  
```

If descending order, use the following one:

```bash
$ curl -X GET 'http://127.0.0.1:8000/metrics/?sort=-clicks' 
 
```

To sort by costs per install, use the following pattern:

```bash
$ curl -X GET 'http://127.0.0.1:8000/metrics/?sort=cpi'  
```
