"""adjust_home_task URL Configuration
"""
from django.contrib import admin
from django.urls import path

from information_metrics.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('metrics/', PerformanceCollection.as_view()),

]
