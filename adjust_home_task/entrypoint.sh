#!/usr/bin/env bash

pip install -r requirements.txt

python manage.py migrate

#Update database with latest datasets from web
python3 manage.py flush --noinput && python3 manage.py generate_dataset

python3 manage.py runserver 0.0.0.0:8000
