"""
Provide implementation of restful endpoint exposes the sample dataset
 through RESTful API which is capable of filtering, grouping and sorting.
"""
from http import HTTPStatus

from django.db.models import FloatField, ExpressionWrapper, F
from django.db.models import Sum
from django.http import JsonResponse
from django.views import View

from information_metrics.mixins.request_parameters import RequestParametersMixin
from information_metrics.models import PerformanceMetrics


class PerformanceCollection(RequestParametersMixin, View):
    """
    Implements performance collection endpoint.
    """

    model = PerformanceMetrics

    def get(self, request, *args, **kwargs):
        """
        Exposed metrics data with filtering, sorting, grouping and cpi calculation.
        """
        try:
            response = self.model.objects.filter(
                **self.filters,
            ).annotate(cpi=ExpressionWrapper(
                    (F('spend') / F('installs')), 
                    output_field=FloatField())
            ).values(
            *self.group_by,
            ).annotate(
                impressions=Sum('impressions'),
            ).annotate(
                clicks=Sum('clicks'),
            ).annotate(
            installs=Sum('installs'),
            ).annotate(
            revenue=Sum('revenue'),
            )

            if self.sort is not None:
                response = response.order_by(self.sort)
        except AttributeError:
            response = self.model.objects.filter(
            ).annotate(cpi=ExpressionWrapper(
                    (F('spend') / F('installs')), 
                    output_field=FloatField())
            ).values()        

        return JsonResponse({'result': list(response)}, status=HTTPStatus.OK)
