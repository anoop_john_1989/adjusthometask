from django.apps import AppConfig


class InformationMetricsConfig(AppConfig):
    name = 'information_metrics'
