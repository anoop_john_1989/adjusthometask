"""
Models for database.
"""
from django.db import models


class PerformanceMetrics(models.Model):
    """
    Model for performance metrics
    """

    date = models.DateField()
    channel = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    os = models.CharField(max_length=255)
    impressions = models.IntegerField()
    clicks = models.IntegerField()
    installs = models.IntegerField()
    spend = models.FloatField()
    revenue = models.FloatField()
