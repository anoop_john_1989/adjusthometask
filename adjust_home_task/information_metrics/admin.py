from django.contrib import admin
from information_metrics.models import PerformanceMetrics

class PerformanceMetricsAdmin(admin.ModelAdmin):
    list_display = ('date','channel','country','os','impressions','clicks','installs','spend','revenue')

admin.site.register(PerformanceMetrics, PerformanceMetricsAdmin)