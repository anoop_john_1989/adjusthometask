"""
Provide mixins for request parameters.
"""
import copy

from django.http import JsonResponse

GET_REQUEST_GROUP_BY_PARAMETER_IS_REQUIRED = \
    'The get request URL is required to contain `group_by` parameter. Please, take a look at documentation.'

NOT_FILTERS_REQUEST_PARAMETER_NAMES = (
    'group_by', 'sort',
)


class RequestParametersMixin:
    """
    Implements incoming request arguments mixin.
    """

    @staticmethod
    def _get_filters_parameters(request_parameters):
        """
        Get filters parameters from request parameters.

        Exclude from request parameters the following parameters: group_by, sort.
        """
        filters = {}
        for parameter, value in request_parameters.items():
            if parameter not in NOT_FILTERS_REQUEST_PARAMETER_NAMES:
                filters.update({
                    parameter: value,
                })
        return filters

    @staticmethod
    def _request_parameters_to_orm_filters(request_parameters):
        """
        Convert request parameters names to fit Django ORM filters names.
        """
        request_parameters = copy.deepcopy(request_parameters)

        date_from = request_parameters.get('date_from')
        date_to = request_parameters.get('date_to')

        if date_from is not None:
            del request_parameters['date_from']
            request_parameters['date__gte'] = date_from

        if date_to is not None:
            del request_parameters['date_to']
            request_parameters['date__lte'] = date_to

        return request_parameters

    def dispatch(self, request, *args, **kwargs):
        """
        Handle incoming request arguments.
        """
        if self.request.GET:
            get_request_parameters = copy.deepcopy(self.request.GET.dict())

            self.sort = self.request.GET.get('sort')
            self.group_by = self.request.GET.getlist('group_by')

            # if not self.group_by:
            #     return JsonResponse({'error': GET_REQUEST_GROUP_BY_PARAMETER_IS_REQUIRED})

            filters_parameters = self._get_filters_parameters(request_parameters=get_request_parameters)
            self.filters = self._request_parameters_to_orm_filters(request_parameters=filters_parameters)

        return super(RequestParametersMixin, self).dispatch(request, *args, **kwargs)
