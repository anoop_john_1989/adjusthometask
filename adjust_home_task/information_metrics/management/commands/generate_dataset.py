"""
Provide management commands for database.
"""
import csv
from datetime import datetime

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.conf import settings

import dateparser
import requests
from information_metrics.models import PerformanceMetrics


class Command(BaseCommand):
    """
    Command to load the performance metrics dataset to the database.
    """

    help = 'Load the performance metrics dataset to the database'

    def handle(self, *args, **options):
        """
        Load the performance metrics dataset to the database.

        Convert date as string from one format to another to fit default Django date's string format.
        """
        user=User.objects.create_user('admin@adjust.com', password='root@123')
        user.is_superuser=True
        user.is_staff=True
        user.save()
        
        url='https://gist.githubusercontent.com/kotik/3baa5f53997cce85cc0336cb1256ba8b/raw/3c2a590b9fb3e9c415a99e56df3ddad5812b292f/dataset.csv'

        response = requests.get(url)

        with open(settings.DATASET_FILE, 'wb') as dataset_file:
            dataset_file.write(response.content)
            
        with open(settings.DATASET_FILE, 'r') as dataset_file:
            csv_reader = csv.DictReader(dataset_file)

            saved_row = 0

            for row_as_ordered_dict in csv_reader:
                row_as_dict = dict(row_as_ordered_dict)

                date_as_object = dateparser.parse(row_as_dict.get('date')).date()
                date_as_string_to_fir_date_field = date_as_object.strftime('%Y-%m-%d')
                row_as_dict['date'] = date_as_string_to_fir_date_field

                performance_metrics = PerformanceMetrics(**row_as_dict)
                performance_metrics.save()

                saved_row += 1

        assert saved_row == PerformanceMetrics.objects.all().count()
